[![PHP from Packagist](https://img.shields.io/packagist/php-v/places2be/osm-tiles)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/5e546b14921046bbb38bb760fa9c2318)](https://www.codacy.com/bb/wirbelwild/places2be-osm-tiles/dashboard)
[![Latest Stable Version](https://poser.pugx.org/places2be/osm-tiles/v/stable)](https://packagist.org/packages/places2be/osm-tiles)
[![Total Downloads](https://poser.pugx.org/places2be/osm-tiles/downloads)](https://packagist.org/packages/places2be/osm-tiles)
[![License](https://poser.pugx.org/places2be/osm-tiles/license)](https://packagist.org/packages/places2be/osm-tiles)

# Places2Be OSM Tiles

Downloads OSM tiles.

## Installation 

This package is made for the use with [Composer](https://packagist.org/packages/places2be/osm-tiles). Add it to your project by running `$ composer require places2be/osm-tiles`. 

## Usage 

Set up the class like that:

```php
<?php

use Places2Be\OSM\Tiles;

$folder = '/path/to/your/tiles';

$servers = [
    'https://a.osm.rrze.fau.de/osmhd/{{zoom}}/{{tileX}}/{{tileY}}.png',
    'https://b.osm.rrze.fau.de/osmhd/{{zoom}}/{{tileX}}/{{tileY}}.png',
    'https://c.osm.rrze.fau.de/osmhd/{{zoom}}/{{tileX}}/{{tileY}}.png',
];

$tiles = new Tiles($folder, $servers); 
```

Note that the server urls contains placeholders.

Download a tile by calling 

```php
<?php

$tiles->getTile(8611, 5640, 14);
``` 

If you have coordinates instead of the `X` and `Y` values, you can convert them by calling 

```php
<?php

use Places2Be\Position\Coordinates;

$coordinates = new Coordinates(48.806035, 9.213587);

$tilesNames = $tiles->getTileNames($coordinates, 14);

$tiles->getTile($tilesNames['x'], $tilesNames['y'], 14);
``` 

Alternatively you can use it like that

```php
<?php

use Places2Be\Position\Coordinates;

$coordinates = new Coordinates(48.806035, 9.213587);

$tiles->getTileFromCoordinates($coordinates, 14);
```

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).