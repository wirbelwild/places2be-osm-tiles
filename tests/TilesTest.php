<?php

/**
 * Places2Be OSM Tiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\OSM\Tests;

use PHPUnit\Framework\TestCase;
use Places2Be\OSM\Tiles;
use ReflectionClass;
use ReflectionException;

/**
 * Tests in class Tiles
 */
class TilesTest extends TestCase
{
    private string $server = 'https://a.osm.rrze.fau.de/osmhd/{{zoom}}/{{tileX}}/{{tileY}}.png';

    private string $cacheFolder = __DIR__ . DIRECTORY_SEPARATOR . 'tmp';

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws ReflectionException
     */
    public function testCanCreateServerUrl(): void
    {
        $tiles = new Tiles(
            $this->cacheFolder,
            [$this->server]
        );
        
        $reflection = new ReflectionClass($tiles::class);
        $method = $reflection->getMethod('getServerUrl');
        $method->setAccessible(true);
        
        $result = $method->invokeArgs($tiles, [1, 2, 3]);
        
        $this->assertEquals(
            'https://a.osm.rrze.fau.de/osmhd/3/1/2.png',
            $result
        );
        
        rmdir($this->cacheFolder);
    }
}
