<?php

/**
 * Places2Be OSM Tiles.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\OSM;

use BitAndBlack\PathInfo\PathInfo;
use Places2Be\Position\Coordinates;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RuntimeException;

/**
 * Requests and downloads tiles
 */
class Tiles implements LoggerAwareInterface
{
    private string $cacheFolder;

    private LoggerInterface $logger;

    /**
     * @param string $cacheFolder Cache folder for tiles.
     * @param array<int, string> $servers Available servers to load the tiles.
     */
    public function __construct(
        string $cacheFolder,
        private array $servers
    ) {
        if (!file_exists($cacheFolder) && !mkdir($cacheFolder, 0777, true) && !is_dir($cacheFolder)) {
            throw new RuntimeException(
                sprintf('Directory "%s" was not created', $cacheFolder)
            );
        }

        $this->cacheFolder = $cacheFolder;
        $this->logger = new NullLogger();
    }

    /**
     * Sets a logger instance
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * Gets a tile by coordinates
     */
    public function getTileFromCoordinates(Coordinates $coordinates, int $zoom): string
    {
        $tileNames = $this->getTileNames($coordinates, $zoom);
        return $this->getTile(
            $tileNames['x'],
            $tileNames['y'],
            $zoom
        );
    }

    public function getTileFileName(int $tileX, int $tileY, int $zoom): string
    {
        $serverUrl = $this->getServerUrl($tileX, $tileY, $zoom);

        $fileName = (string) parse_url($serverUrl, PHP_URL_PATH);
        $fileExtension = PathInfo::createFromFile($fileName)->getExtension();

        return $zoom . '-' . $tileX . '-' . $tileY . '.' . $fileExtension;
    }

    public function getTileFilePath(int $tileX, int $tileY, int $zoom): string
    {
        return $this->cacheFolder . DIRECTORY_SEPARATOR . $this->getTileFileName($tileX, $tileY, $zoom);
    }

    public function isTileExisting(int $tileX, int $tileY, int $zoom): bool
    {
        $tileFilePath = $this->getTileFilePath($tileX, $tileY, $zoom);
        return file_exists($tileFilePath);
    }

    /**
     * Gets a tile
     */
    public function getTile(int $tileX, int $tileY, int $zoom, int $try = 5): string
    {
        $serverUrl = $this->getServerUrl($tileX, $tileY, $zoom);
        $fileNameLocal = $this->getTileFileName($tileX, $tileY, $zoom);

        if ($this->isTileExisting($tileX, $tileY, $zoom)
            || $this->hasFileFromServer($serverUrl, $fileNameLocal)
        ) {
            $this->logger->notice('Returned tile "' . $fileNameLocal . '"');
            return $this->cacheFolder . DIRECTORY_SEPARATOR . $fileNameLocal;
        }

        --$try;

        if ($try > 0) {
            return $this->getTile($tileX, $tileY, $zoom, $try);
        }

        return '';
    }

    /**
     * Takes a server url randomly and inserts the values to the placeholders
     */
    private function getServerUrl(int $tileX, int $tileY, int $zoom): string
    {
        $randomServerUrl = $this->servers[array_rand($this->servers)];

        return str_replace(
            [
                '{{tileX}}',
                '{{tileY}}',
                '{{zoom}}',
            ],
            [
                $tileX,
                $tileY,
                $zoom,
            ],
            $randomServerUrl
        );
    }

    /**
     * Gets a tiles name from coordinates
     *
     * Please not that the tile names use a different logic that coordinates:
     * When you get a tile name from given coordinates you'll receive the name
     * of the tile that contains the coordinates
     *
     * @param Coordinates $coordinates
     * @param int $zoom
     * @return array<string, int>
     */
    public function getTileNames(Coordinates $coordinates, int $zoom): array
    {
        return [
            'x' => (int) floor((($coordinates->getLongitude() + 180) / 360) * (2 ** $zoom)),
            'y' => (int) floor((1 - log(tan(deg2rad($coordinates->getLatitude())) + 1 / cos(deg2rad($coordinates->getLatitude()))) / M_PI) / 2 * (2 ** $zoom)),
        ];
    }

    /**
     * Loads a tile from a server
     */
    private function hasFileFromServer(string $serverUrl, string $fileNameLocal): bool
    {
        $fileContent = @file_get_contents($serverUrl);

        if (false === $fileContent) {
            return false;
        }

        $file = fopen($this->cacheFolder . DIRECTORY_SEPARATOR . $fileNameLocal, 'wb+');
        
        if (false === $file) {
            return false;
        }
        
        fwrite($file, $fileContent);
        fclose($file);
        return true;
    }
}
