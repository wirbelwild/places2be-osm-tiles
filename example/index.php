<?php


use Monolog\Handler\StreamHandler;

require '../vendor/autoload.php';

use Monolog\Logger;
use Places2Be\OSM\Tiles;

$stream = new StreamHandler('php://stdout', Logger::DEBUG);
$logger = new Logger('Tiles Test');
$logger->pushHandler($stream);

$folder = __DIR__ . DIRECTORY_SEPARATOR . 'tiles';

if (!file_exists($folder) && !mkdir($folder, 0777, true) && !is_dir($folder)) {
    throw new RuntimeException(sprintf('Directory "%s" was not created', $folder));
}

$servers = [
    'https://a.osm.rrze.fau.de/osmhd/{{zoom}}/{{tileX}}/{{tileY}}.png',
    'https://b.osm.rrze.fau.de/osmhd/{{zoom}}/{{tileX}}/{{tileY}}.png',
    'https://c.osm.rrze.fau.de/osmhd/{{zoom}}/{{tileX}}/{{tileY}}.png',
];

$tiles = new Tiles($folder, $servers);
$tiles->setLogger($logger);

var_dump($tiles->getTile(8611, 5640, 14));
